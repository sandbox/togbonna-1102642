// $Id
SIMPLECHARTS
============

-- SUMMARY --

This module was motivated by a lack of a non-developer user-friendly charting
solution amongs contribs modules when I needed one for a project.

This module makes it possible for users to create nodes that display various kinds of
charts. It also creates a configurable block that displays a chart.

It extends the Charts and Graphs (charts_graphs) module, therefore that module 
must be set up correctly before you can use this one successfully. i.e. It 
depends on charts_graphs module and its dependencies.

For a full description of the module, visit the project page:
  http://drupal.org/project/simplecharts

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/simplecharts


-- REQUIREMENTS --

* charts_graphs module and its dependencies. The charts_graphs module 
requires that you download and install some third party libraries such as 
amCharts <http://www.amcharts.com/> and others. At least one of these must 
be installed. Some may be packaged with that module by default. Follow the instruction 
for that module.


-- INSTALLATION --

* Download, install, enable and configure the Charts and Graphs (charts_graphs) module 
correctly. Follow its installation instruction.

* Extract this module into your contribs modules folder. see http://drupal.org/node/70151 
for further information.

* Go to your modules page (YOURSITE/admin/modules) and enable simplecharts and save 
the configuration.


-- CONFIGURATION --

* IMPORTANT: Go to the SimpleCharts configuration page (YOURSITE/admin/settings/simplecharts), 
 select a chart provider plugin. NOTE: charts_graphs module must be set up correctly 
 for the chart provider plugin options to show.
 
* Add the desired height and width of charts that will be created as nodes. 

* Save the settings. 

* Additionally, to display your charts in a block 

    * * Go to the block page (YOURSITE/admin/build/block).
    
    * * Locate the SimpleCharts entry and click its configure link.

    * * Enter your desired block width and height (or just accept the default).
    
    * * Select the node to display and save your settings (or just accept the default). 
    
    * * NOTE: Some SimpleCharts nodes should have been created previously before this step.


-- USAGE --

* After successfully installing and configuring, go to 'Create Content', select 'SimpleCharts', 
select and fill-in the following:
    * * Chart type: The type of chart you would like to create. Bar, Pie, Line, etc.
    
    * * X labels: Title for each corresponding data item in the Data set in the form of a comma 
        separated list. Example: Label1,Label2,Label3... Must match the number of items 
        in the data sets.
    
    * * Data set 1: Numeric data items to be plotted in the chart in the form of a comma 
    separated list of numbers. Example: 123,234,345... Must be numbers. Must match the number of Labels. Allowed range: 1 - 999999.
    
    * * Data Set 1 legend name: Legend name for Data Set 1.
    
    * * Data set 1: A second set of data items to be plotted in the chart in the form of a comma 
    separated list of numbers. Example: 123,234,345... Must be numbers. Must match the number of Labels. Allowed range: 1 - 999999.
    
    
    * * Data Set 1 legend name: Legend name for Data Set 2.
    
    * * Y legend: Label for the Y-axis of chart.

* Save to create your node.

* To display your chart as a block see -- CONFIGURATION -- instructions above.

-- TROUBLESHOOTING --

* After creating a SimpleChart node the chart does not show:
   
    * * Make sure Charts and Graphs (charts_graphs) module is installed correctly.
    
    * * Make sure you have visited and saved the configuration page 
        (YOURSITE/admin/settings/simplecharts) at least once.

* After placing SimpleChart block in a region the chart does not display:
   
    * * Make sure that at least one SimpleChart node has been created successfully.
    
    * * Make sure you have visited and saved the block configuration page at least once.

* Chart provider plugin option in the admin configuration page is empty:

    * * Make sure Charts and Graphs (charts_graphs) module is installed correctly.
        Especially that at least one third party library was installed with it. Buff 
        and Google Charts libraries ships with it by default. Google Charts requires 
        that you are online to work.
        