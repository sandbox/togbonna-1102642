<?php
// $Id$
/**
 * @author Tony Ogbonna http://drupal.org/user/867704
 * @file
 * Settings function for SimpleCharts module.
 */

/**
 * Module settings page.
 *
 * @ingroup form
 */
function _simplecharts_settings_page() {
  $form = array();
  _simplecharts_settings_form($form, array());

  $form['submit'] = array(
    '#type'           => 'submit',
    '#value'          => t('Save settings'),
  );

  return $form;
}

/**
 * Module settings page.
 *
 * @ingroup form
 */
function _simplecharts_settings_form(&$form, $default = array(), $options = array()) {
  $default = $default + _simplecharts_settings();
  asort($default['#plugins']);
  // asort($default['#types']);

  $form['plugin'] = array(
    '#default_value'  => $default['#plugin'],
    '#options'        => $default['#plugins'],
    '#required'       => TRUE,
    '#type'           => 'select',
    '#title'          => t('Chart Provider plugin'),
  );
  $form['size'] = array(
    '#description'    => t('The chart size, in pixels'),
    '#type'           => 'fieldset',
    '#title'          => t('Size'),
  );
  $form['size']['width'] = array(
    '#default_value'  => isset($default['#width']) ?  $default['#width'] : 450,
    '#required'       => TRUE,
    '#size'           => 8,
    '#type'           => 'textfield',
    '#title'          => t('Width'),
  );
  $form['size']['height'] = array(
    '#default_value'  => isset($default['#height']) ?  $default['#height'] : 450,
    '#required'       => TRUE,
    '#size'           => 8,
    '#type'           => 'textfield',
    '#title'          => t('Height'),
  );

  return $form;
}

/**
 * Module settings page.
 *
 * @ingroup form
 */
function _simplecharts_settings() {
  // Get the previously saved data from Data Base
  static $default = array();

  if (empty($default)) {
    $default = variable_get('simplecharts_settings', array());

    // Query for all charting providers info
    $plugins = charts_graphs_apis();

    foreach ($plugins as $plugin) {
      $default['#plugins'][$plugin->name] = $plugin->name;
    }
    asort($default['#plugins']);
    $default['#plugin'] = empty($default['#plugin']) ? next(array_keys($default['#plugins'])) : $default['#plugin'];

  }

  return $default;
}

/**
 * Module settings page.
 *
 * @ingroup form
 */
function _simplecharts_settings_page_submit(&$form, &$form_state) {
  $settings = $form_state['values'];

  // Discart all form values non related to charts
  unset($settings['submit']);
  unset($settings['form_id']);
  unset($settings['form_build_id']);
  unset($settings['form_token']);
  unset($settings['op']);

  // Unwanted values
  unset($settings['color']['color_palette']);

  // Include a # sign in all attributes, because it will make the
  // merge between the chart data and the defaults easier on every
  // chart display.
  foreach ($settings as $index => $value) {
    $default['#'. $index] = $value;
  }

  // Save the data into database
  variable_set('simplecharts_settings', $default);
}
