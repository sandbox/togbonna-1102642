<?php
// $Id$

/**
 * @file
 * simplecharts.module - create chart display nodes and a configuable block.
 */

/**
 * Implementation of hook_help().
 */
function simplecharts_help($path, $arg) {
  switch ($path) {
    case 'node/add/simplecharts':
      return variable_get('simplecharts_help', 'Create a node that displays charts.');
    case 'admin/settings/simplecharts':
      $output = '<p>' . t('Setup your Simplechart defaults here. Accept the default at least') .'</p>';
      return $output;
    case 'admin/help#simplecharts':
      // Return a line-break version of the module README.txt
      return filter_filter('process', 1, NULL, file_get_contents( dirname(__FILE__) . "/README.txt"));
    default:
  }
}

/**
 * Implementation of hook_menu().
 */
function simplecharts_menu() {
  $items = array();

  $items['admin/settings/simplecharts'] = array(
    'title' => 'SimpleCharts',
    'description' => 'Configure your SimpleChart settings.',
    'page callback' => 'drupal_get_form',
    'page arguments'  => array('_simplecharts_settings_page'),
    'access arguments'  => array('configure simplecharts default settings'),
    'file' => 'simplecharts.admin.inc',
  );

  return $items;
}

/**
 * Implementation of hook_node_info().
 */
function simplecharts_node_info() {
  return array(
    'simplecharts' => array(
      'name' => t('SimpleChart'),
      'module' => 'simplecharts',
      'description' => t("Create a node with a chart display."),
      'has_title' => TRUE,
      'title_label' => t('Title'),
      'has_body' => TRUE,
      'body_label' => t('Description'),
    )
  );
}

/**
 * Implementation of hook_access().
 *
 * Node modules may implement node_access() to determine the operations
 * users may perform on nodes. This example uses a very common access pattern.
 */
function simplecharts_access($op, $node, $account) {
  if ($op == 'create') {
    return user_access('create simplecharts content', $account);
  }

  if ($op == 'update') {
    if (user_access('edit any simplecharts content', $account) || (user_access('edit own simplecharts content', $account) && ($account->uid == $node->uid))) {
      return TRUE;
    }
  }

  if ($op == 'delete') {
    if (user_access('delete any simplecharts content', $account) || (user_access('delete own simplecharts content', $account) && ($account->uid == $node->uid))) {
      return TRUE;
    }
  }
}

/**
 * Implementation of hook_perm().
 *
 * Since we are limiting the ability to create new nodes to certain users,
 * we need to define what those permissions are here. */
function simplecharts_perm() {
  return array(
    'create simplecharts content',
    'delete own simplecharts content',
    'delete any simplecharts content',
    'edit own simplecharts content',
    'edit any simplecharts content',
    'view simplecharts display',
    'configure simplecharts default settings',
  );
}

/**
 * Implementation of hook_form().
 *
 * Now it's time to describe the form for collecting the information
 * specific to this node type. This hook requires us to return an array with
 * a sub array containing information for each element in the form.
 */
function simplecharts_form(&$node, $form_state) {
  // The site admin can decide if this node type has a title and body, and how
  // the fields should be labeled. We need to load these settings so we can
  // build the node form correctly.
  $type = node_get_types('type', $node);

  if ($type->has_title) {
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => check_plain($type->title_label),
      '#required' => TRUE,
      '#default_value' => $node->title,
      '#weight' => -5
    );
  }

  if ($type->has_body) {
    // In Drupal 6, we use node_body_field() to get the body and filter
    // elements. This replaces the old textarea + filter_form() method of
    // setting this up. It will also ensure the teaser splitter gets set up
    // properly.
    $form['body_field'] = node_body_field($node, $type->body_label, $type->min_word_count);
  }

  // Get default chart type set by admin
  $default = variable_get('simplecharts_settings', array());
  // Get the active chart provider and get chart types it implements
  $providers = charts_graphs_apis();
  $provider_class = $providers[$default['#plugin']];

  // Now we define the form elements specific to our node type.
  $form['chart_type'] = array(
    '#default_value'  => isset($node->chart_type) ? $node->chart_type : '',
    '#options'        => $provider_class->chart_types,
    '#required'       => TRUE,
    '#type'           => 'select',
    '#title'          => t('Chart type'),
  );
  $form['labels'] = array(
    '#type' => 'textfield',
    '#required'       => TRUE,
    '#title' => t('X labels'),
    '#default_value' => isset($node->labels) ? $node->labels : '',
    '#description' => t('Label title for each corresponding data item in the Data set. Format: A comma separated list of item. Example: Label1,Label2,Label3... Must match the number of items in the data sets.'),
  );
  $form['dataset1'] = array(
    '#type' => 'textfield',
    '#required'       => TRUE,
    '#title' => t('Data set 1'),
    '#default_value' => isset($node->dataset1) ? $node->dataset1 : '',
    '#description' => t('Data items to be plotted in the chart in the form of a comma separated list of numbers. Example: 123,234,345... Must be numeric. Must match the number of Labels. Allowed range: 1 - 999999.'),
  );
  $form['legend_name1'] = array(
    '#type' => 'textfield',
    '#title' => t('Data Set 1 legend name'),
    '#default_value' => isset($node->labels) ? $node->legend_name1 : '',
    '#description' => t('Legend name for Data Set 1.'),
  ); 
  $form['dataset2'] = array(
    '#type' => 'textfield',
    '#title' => t('Data set 2'),
    '#default_value' => isset($node->dataset2) ? $node->dataset2 : '',
    '#description' => t('A second set of data items to be plotted in the chart in the form of a comma separated list of numbers.. Example: 123,234,345... Must be numeric. Must match the number of Labels. Allowed range: 1 - 999999.'),
  );
  $form['legend_name2'] = array(
    '#type' => 'textfield',
    '#title' => t('Data Set 2 legend name'),
    '#default_value' => isset($node->labels) ? $node->legend_name2 : '',
    '#description' => t('Legend name for Data Set 2.'),
  );
  $form['legend'] = array(
    '#type' => 'textfield',
    '#title' => t('Y legend'),
    '#default_value' => isset($node->legend) ? $node->legend : '',
    '#description' => t('Label for the Y-axis of chart'),
  );
  $form['plugin'] = array(
    '#type' => 'hidden',
    '#title' => t('Plugin'),
    '#value' => $default['#plugin'],
  );

  return $form;
}

/**
 * Implementation of hook_validate().
 *
 * Errors should be signaled with form_set_error().
 */
function simplecharts_validate($node, &$form) {
  if (empty($node->dataset1)) {
    form_set_error('dataset1', t('Data Set 1 must not be empty.'));
  }
  if (empty($node->chart_type)) {
    form_set_error('chart_type', t('You must chose a Chart Type.'));
  }
  $data = explode(',', $node->dataset1);
  $labs = explode(',', $node->labels);
  // print_r($node->dataset1);
  $numeric = TRUE;
  foreach ($data as $item) {
    if (!is_numeric($item) OR $item < 0 OR $item > 999999) {
      $numeric = FALSE;
    }
  }
  if ($numeric == FALSE) {
    form_set_error('dataset1', t('Data Set 1 must contain only numeric values betwen 0 and 999999 inclusive.'));
  }
  if (count($labs) != count($data)) {
    form_set_error('dataset1', t('The number of labels items must match the number of Data Set 1 items.'));
  }

  if (!in_array($node->chart_type, array('pie', 'pie_3d')) && !empty($node->chart_type) && !empty($node->dataset2)) {
    $data2 = explode(',', $node->dataset2);
    $numeric = TRUE;
    foreach ($data2 as $item) {
      if (!is_numeric($item) OR $item < 0 OR $item > 999999) {
        $numeric = FALSE;
      }
    }
    if ($numeric == FALSE) {
      form_set_error('dataset2', t('Data Set 2 must contain only numeric values betwen 0 and 999999 inclusive.'));
    }
    if (count($labs) != count($data2)) {
      form_set_error('dataset2', t('The number of Data Set 2 items must match the number of labels items.'));
    }
    /*
  	if (empty($node->dataset2)) {
      form_set_error('dataset2', t('The chart type you chose requires that Data Set 2 must not be empty.'));
    }
    */
    $data2 = explode(',', $node->dataset2);
    $numeric = TRUE;
    foreach ($data2 as $item) {
      if (!is_numeric($item) OR $item < 0 OR $item > 999999) {
        $numeric = FALSE;
      }
    }
    if ($numeric == FALSE) {
      form_set_error('dataset2', t('Data Set 2 must contain only numeric values betwwen 0 and 999999 inclusive.'));
    }
  }
}

/**
 * Implementation of hook_insert().
 *
 * As a new node is being inserted into the database, we need to do our own
 * database inserts.
 */
function simplecharts_insert($node) {
  db_query("INSERT INTO {simplecharts} (vid, nid, chart_type, labels, dataset1, dataset2, legend_name1, legend_name2, plugin, legend) VALUES (%d, %d, '%s','%s','%s','%s','%s','%s','%s','%s')", $node->vid, $node->nid, $node->chart_type, $node->labels, $node->dataset1, $node->dataset2, $node->legend_name1, $node->legend_name2, $node->plugin, $node->legend);
}

/**
 * Implementation of hook_update().
 *
 * As an existing node is being updated in the database, we need to do our own
 * database updates.
 */
function simplecharts_update($node) {
  // if this is a new node or we're adding a new revision,
  if ($node->revision) {
    simplecharts_insert($node);
  }
  else {
    db_query("UPDATE {simplecharts} SET chart_type = '%s', labels = '%s', dataset1 = '%s', dataset2 = '%s', legend_name1 = '%s', legend_name2 = '%s', plugin = '%s', legend = '%s' WHERE vid = %d", $node->chart_type, $node->labels, $node->dataset1, $node->dataset2, $node->legend_name1, $node->legend_name2, $node->plugin, $node->legend, $node->vid);
  }
}

/**
 * Implementation of hook_nodeapi().
 *
 * When a node revision is deleted, we need to remove the corresponding record
 * from our table. The only way to handle revision deletion is by implementing
 * hook_nodeapi().
 */
function simplecharts_nodeapi(&$node, $op, $teaser, $page) {
  switch ($op) {
    case 'delete revision':
      // Notice that we're matching a single revision based on the node's vid.
      db_query('DELETE FROM {simplecharts} WHERE vid = %d', $node->vid);
      break;
  }
}

/**
 * Implementation of hook_delete().
 *
 * When a node is deleted, we need to remove all related records from our table.
 */
function simplecharts_delete($node) {
  // Notice that we're matching all revision, by using the node's nid.
  db_query('DELETE FROM {simplecharts} WHERE nid = %d', $node->nid);
}

/**
 * Implementation of hook_load().
 *
 * Now that we've defined how to manage the node data in the database, we
 * need to tell Drupal how to get the node back out. This hook is called
 * every time a node is loaded, and allows us to do some loading of our own.
 */
function simplecharts_load($node) {
  $additions = db_fetch_object(db_query('SELECT chart_type, labels, dataset1, dataset2, legend_name1, legend_name2, plugin, legend FROM {simplecharts} WHERE vid = %d', $node->vid));
  return $additions;
}

/**
 * Implementation of hook_view().
 *
 * This is a typical implementation that simply runs the node text through
 * the output filters.
 */
function simplecharts_view($node, $teaser = FALSE, $page = FALSE) {
  $node = node_prepare($node, $teaser);
  $node->content['myfield'] = array(
    '#value' => theme('simplecharts_view_info', $node),
    '#weight' => 1,
  );

  return $node;
}

/**
 * Implementation of hook_theme().
 *
 * This lets us tell Drupal about our theme functions and their arguments.
 */
function simplecharts_theme() {
  return array(
    'simplecharts_view_info' => array(
      'arguments' => array('node'),
    ),
  );
}

/**
 * A custom theme function.
 *
 * By using this function to format our node-specific information, themes
 * can override this presentation if they wish. We also wrap the default
 * presentation in a CSS class that is prefixed by the module name. This
 * way, style sheets can modify the output without requiring theme code.
 */
function theme_simplecharts_view_info($node) {
  $default = variable_get('simplecharts_settings', array());
  // $dim = explode(',',$node->field_dimension[0]['value']);
  $labels = explode(',', $node->labels);
  $dataset1 = explode(',', $node->dataset1);
  $dataset2 = explode(',', $node->dataset2);
  $canvas = charts_graphs_get_graph($node->plugin);

  $canvas->title = $node->title;
  $canvas->type = $node->chart_type; // a chart type supported by the charting engine. See further in the doc for the list.
  $canvas->colour = '#cdcdcd';
  $canvas->height = $default['#height'];
  $canvas->width  = $default['#width'];;
  $canvas->theme = 'keynote';
  $data_desc = $node->description;

  if (in_array($node->chart_type, array('pie', 'pie_3d'))) {
    $canvas->series = array(
      "$node->legend_name1" => $dataset1,
    );
  }
  else {
    $canvas->series = array(
      "$node->legend_name2" => $dataset2,
      "$node->legend_name1" => $dataset1,
    );
  }
  $canvas->x_labels = $labels;
  // $block['content'] = $canvas->get_chart();

  $output = '<div class="simplecharts_view_info">';
  $output .= $canvas->get_chart();
  $output .= '</div>';
  return $output;
}
/**
 * Implementation of hook_block().
 */

function simplecharts_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks[0]['info'] = t('SimpleCharts');
      $blocks[0]['cache'] = BLOCK_NO_CACHE;

      return $blocks;
    case 'view':
      if ($delta == 0 && user_access('view simplecharts display')) {
        $default = variable_get('simplecharts_settings', array());
        $node_to_show = $default['#block_node_to_show'];

        $sql = "SELECT n.nid FROM {node} n WHERE type = '%s' AND status = %d ORDER BY n.created DESC";

        if ($node_to_show == 2) {
          $result = db_query(db_rewrite_sql($sql), 'simplecharts', 1);

          $rand = rand(0, $result->num_rows-1);
          $num = 0;
          while ($data = db_fetch_object($result)) {
            if ($num == $rand) {
              $nid = $data->nid;
              break;
            }
            $num++;
          }

        }
        elseif ($node_to_show == 1) {
          $sql = "SELECT n.nid FROM {node} n WHERE type = '%s' AND status = %d ORDER BY n.created ASC";
          $nid = db_result(db_query_range(db_rewrite_sql($sql), 'simplecharts', 1, 0, 1));
        }
        else {
          $nid = db_result(db_query_range(db_rewrite_sql($sql), 'simplecharts', 1, 0, 1));
        }

        if ($nid) {
          $node = node_load($nid);

          $labels = explode(',', $node->labels);
          $dataset1 = explode(',', $node->dataset1);
          $dataset2 = explode(',', $node->dataset2);
          $canvas = charts_graphs_get_graph($node->plugin);

          $canvas->title = $node->title;
          $canvas->type = $node->chart_type; // a chart type supported by the charting engine. See further in the doc for the list.
          $canvas->colour = '#cdcdcd';
          $canvas->height = $default['#block_height'];
          $canvas->width  = $default['#block_width'];
          $canvas->theme = 'keynote';
          $data_desc = $node->description;

          if (in_array($node->chart_type, array('pie', 'pie_3d'))) {
            $canvas->series = array(
              "$node->legend_name1" => $dataset1,
            );
          }
          else {
            $canvas->series = array(
              "$node->legend_name2" => $dataset2,
              "$node->legend_name1" => $dataset1,
            );
          }

          $canvas->x_labels = $labels;

          $block['subject'] = t($node->title);
          $block['content'] = $canvas->get_chart();
        }
      }
      return $block;
    case 'configure':
      // If $op is "configure", we need to provide the administrator with a
      // configuration form. The $delta parameter tells us which block is being configured.
      $form = array();
      if ($delta == 0) {
        $default = variable_get('simplecharts_settings', array());
        $form['size'] = array(
          '#description' => t('The SimpleCharts block size, in pixels'),
          '#type' => 'fieldset',
          '#title' => t('Size'),
        );
        $form['size']['block_width'] = array(
          '#default_value' => isset($default['#block_width']) ?  $default['#block_width'] : 300,
          '#required' => TRUE,
          '#size' => 8,
          '#type' => 'textfield',
          '#title' => t('Block width'),
          '#description' => t('Value must be between 100 - 800 pixels'),
        );
        $form['size']['block_height'] = array(
          '#default_value' => isset($default['#block_height']) ?  $default['#block_height'] : 300,
          '#required' => TRUE,
          '#size' => 8,
          '#type' => 'textfield',
          '#title' => t('Block height'),
          '#description' => t('Value must be between 100 - 800 pixels'),
        );
        $form['block_node_to_show'] = array(
          '#options' => array('Latest', 'Oldest', 'Random'),
          '#required' => TRUE,
          '#type' => 'select',
          '#title' => t('SimpleCharts content to display'),
          '#default_value' => isset($default['#block_node_to_show']) ? $default['#block_node_to_show'] : 0,
        );
      }
      return $form;
    case 'save':
      // If $op is "save", we need to save settings from the configuration form.
      // Since the first block is the only one that allows configuration, we
      // need to check $delta to make sure we only save it.
      if ($delta == 0) {
        if ($edit['block_width'] < 100 || $edit['block_width'] > 800) {
          $edit['block_width'] = 300;
        }
        if ($edit['block_height'] < 100 || $edit['block_height'] > 800) {
          $edit['block_height'] = 300;
        }

        $default = variable_get('simplecharts_settings', array());

        foreach ($edit as $index => $value) {
          $default['#'. $index] = $value;
        }

        // Save the data into database
        variable_set('simplecharts_settings', $default);
      }
      return;
  }
}

/**
 * @} End of "simplecharts".
 */